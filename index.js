#!/usr/bin/env node
/* eslint-env node */
/* eslint-disable no-console */
const cli = require('cli');
const fs = require('fs');
const jmp = require('json-merge-patch');

const options = cli.parse({
  patch: ['p', 'Patch file', 'file', null],
  source: ['s', 'Source file', 'file', null],
  output: ['o', 'Output file, Defaults to source', 'file', null],
  dryrun: ['d', 'Dry Run', 'bool', false],
});

options.output = options.output || options.source;

const patch = JSON.parse(fs.readFileSync(options.patch, 'utf8'));
const source = JSON.parse(fs.readFileSync(options.source, 'utf8'));
const merged = jmp.apply(source, patch);
const output = JSON.stringify(merged, null, 2);

if (options.dryrun) {
  cli.output(`Merge: ${output}`);
} else {
  fs.writeFileSync(options.output, output);
  cli.output(`Patch from ${options.patch} merged to ${options.output}`);
}

