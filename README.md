# json-merge-patch

Merges two JSON files together.

## Example:

```js
// Patch
{
  "foo": "PATCH FOO",
  "bar": "baz",
  "biff": null
}
```

```js
// Source i.e. target
{
  "foo": "foo",
  "biff": "bones"
}
```

```
json-merge-patch -p patch.json -s source.json
```

```js
// Result
{
  "foo": "PATCH FOO",
  "bar": "baz"
}
```

Works deeply. YMMV with arrays. Great for merging bower.json

TBA: support for json-merge-patch library's other commands.
